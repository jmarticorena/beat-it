﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Kinect = Windows.Kinect;
using UnityEngine.UI;
using System;

public class BodySourceView : MonoBehaviour 
{
    public Material BoneMaterial;
    public GameObject BodySourceManager;
    
    private Dictionary<ulong, GameObject> _Bodies = new Dictionary<ulong, GameObject>();
    private BodySourceManager _BodyManager;

    static float get_adyacent(float opossite, float angle_to_x)
    {
        double radians = angle_to_x * (Math.PI / 180);
        return (float)(opossite / (Math.Tan(radians)));

    }

    public GameObject bass_drum;
    private static float bass_drum_distance = 4; // distance = z
    private static float bass_drum_angle = 90;

    public GameObject ride;
    private static float ride_distance = 4.5f;
    private static float ride_angle = 40;
    public GameObject crash;
    private static float crash_distance = 4f;
    private static float crash_angle = 35 + 90;
    public GameObject hit_hat;
    private static float hit_hat_distance = 1.8f;
    private static float hit_hat_angle = 54f + 90;

    public GameObject floor_tom;
    private static float floor_tom_distance = 2;
    private static float floor_tom_angle = 33.75f;
    public GameObject high_tom;
    private static float high_tom_distance = 4.5f;
    private static float high_tom_angle = 90 + 15.5f;
    public GameObject middle_tom;
    private static float middle_tom_distance = 4.5f;
    private static float middle_tom_angle = 67.5f;
    public GameObject snare_drum;
    private static float snare_drum_distance = 2;
    private static float snare_drum_angle = 15.5f + 90;
    public GameObject stend;

    public GameObject pedal;
    private static float pedal_distance = 3f;
    private static float pedal_angle = (floor_tom_angle + snare_drum_angle) / 2;

    public Transform panel;


    GameObject CreateObject( GameObject b, float x, float y, float z)
    {
        GameObject a = (GameObject)Instantiate(b);
        a.transform.position = new Vector3(x, y, z);
        a.transform.SetParent(panel.transform, false);
        return a;
    }

    private Dictionary<Kinect.JointType, Kinect.JointType> _BoneMap = new Dictionary<Kinect.JointType, Kinect.JointType>()
    {
        { Kinect.JointType.FootLeft, Kinect.JointType.AnkleLeft },
        { Kinect.JointType.AnkleLeft, Kinect.JointType.KneeLeft },
        { Kinect.JointType.KneeLeft, Kinect.JointType.HipLeft },
        { Kinect.JointType.HipLeft, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.FootRight, Kinect.JointType.AnkleRight },
        { Kinect.JointType.AnkleRight, Kinect.JointType.KneeRight },
        { Kinect.JointType.KneeRight, Kinect.JointType.HipRight },
        { Kinect.JointType.HipRight, Kinect.JointType.SpineBase },
        
        { Kinect.JointType.HandTipLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.ThumbLeft, Kinect.JointType.HandLeft },
        { Kinect.JointType.HandLeft, Kinect.JointType.WristLeft },
        { Kinect.JointType.WristLeft, Kinect.JointType.ElbowLeft },
        { Kinect.JointType.ElbowLeft, Kinect.JointType.ShoulderLeft },
        { Kinect.JointType.ShoulderLeft, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.HandTipRight, Kinect.JointType.HandRight },
        { Kinect.JointType.ThumbRight, Kinect.JointType.HandRight },
        { Kinect.JointType.HandRight, Kinect.JointType.WristRight },
        { Kinect.JointType.WristRight, Kinect.JointType.ElbowRight },
        { Kinect.JointType.ElbowRight, Kinect.JointType.ShoulderRight },
        { Kinect.JointType.ShoulderRight, Kinect.JointType.SpineShoulder },
        
        { Kinect.JointType.SpineBase, Kinect.JointType.SpineMid },
        { Kinect.JointType.SpineMid, Kinect.JointType.SpineShoulder },
        { Kinect.JointType.SpineShoulder, Kinect.JointType.Neck },
        { Kinect.JointType.Neck, Kinect.JointType.Head },
    };

    private float my_z = 0;
    private float my_x= 0;

    private float hand_r_y = 0;
    private float hand_l_y= 0;
    private float shoulder_r_y = 0;
    private float shoulder_l_y = 0;
    private float elbow_r_y = 0;
    private float elbow_l_y = 0;

    private float hand_l_x = 0;
    private float shoulder_l_x = 0;

    private float arm_lenght = 0;

    private float final_x = 0;
    private float final_y = 0;
    private float final_z = 0;

    private float error_x = 3;
    private float error_arm_y = 1;

    private bool drums_placed = false;

    private Text txtRef;
    private Text txtRef_feed;

    private bool ready = false;
    private bool person_is_centered = false;
    private bool got_arm_lenght = false;

    private void Start()
    {
        txtRef = GameObject.Find("Help").GetComponent<Text>();
        txtRef_feed = GameObject.Find("Feedback").GetComponent<Text>();
    }

    void Update () 
    {
        if (BodySourceManager == null)
        {
            return;
        }
        
        _BodyManager = BodySourceManager.GetComponent<BodySourceManager>();
        if (_BodyManager == null)
        {
            return;
        }
        
        Kinect.Body[] data = _BodyManager.GetData();
        if (data == null)
        {
            return;
        }

        List<ulong> trackedIds = new List<ulong>();
        foreach(var body in data)
        {
            if (body == null)
            {
                continue;
              }
                
            if(body.IsTracked)
            {
                trackedIds.Add (body.TrackingId);
            }
        }
        
        List<ulong> knownIds = new List<ulong>(_Bodies.Keys);
        
        // First delete untracked bodies
        foreach(ulong trackingId in knownIds)
        {
            if(!trackedIds.Contains(trackingId))
            {
                Destroy(_Bodies[trackingId]);
                _Bodies.Remove(trackingId);
            }
        }

        int counter = 0;


        if (!ready)
        {
            foreach (var body in data)
            {
                /*if (body == null)
                {
                    continue;
                }*/

                if (body != null && body.IsTracked)
                {
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////
                    txtRef.text = "Usuario encontrado...";
                    counter++;
                    bool theres_head = false;
                    bool theres_leftF = false;
                    bool theres_rightF = false;

                    for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
                    {
                        Kinect.Joint sourceJoint = body.Joints[jt];
                        Vector3 localPosition = GetVector3FromJoint(sourceJoint);
                        if (jt.ToString() == "SpineBase")
                        {
                            my_x = localPosition.x;
                            my_z = localPosition.z;
                        }

                        if (jt.ToString() == "Head" && body.Joints[jt].TrackingState.ToString() == "Tracked")
                        {
                            theres_head = true;
                        }
                        if (jt.ToString() == "FootLeft" && body.Joints[jt].TrackingState.ToString() == "Tracked")
                        {
                            theres_leftF = true;
                        }
                        if (jt.ToString() == "FootRight" && body.Joints[jt].TrackingState.ToString() == "Tracked")
                        {
                            theres_rightF = true;
                        }
                    }

                    string feedback = "";
                    if (theres_head)
                    {
                        if (!theres_leftF && !theres_rightF)
                        {
                            feedback += "No se puede detectar los pies.\n Hazte un poco más atras o verifica la posicion de tu kinect.";
                            ready = false;
                        }
                        else
                        {
                            feedback += "Perfecto, pudimos detectar todo el cuerpo.";
                            ready = true;
                        }
                    }
                    else
                    {
                        if (!theres_leftF && !theres_rightF)
                        {
                            feedback += "No se puede detectar la cabeza ni los pies.\n Hazte un poco más atras o verifica la posicion de tu kinect.";
                            ready = false;
                        }
                        else
                        {
                            feedback += "No se puede detectar la cabeza.\n Hazte un poco más atras o verifica la posicion de tu kinect.";
                            ready = false;
                        }
                    }
                    txtRef_feed.text = feedback;
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////

                    /*if (!_Bodies.ContainsKey(body.TrackingId))
                    {
                        _Bodies[body.TrackingId] = CreateBodyObject(body.TrackingId);
                    }

                    RefreshBodyObject(body, _Bodies[body.TrackingId]);*/

                }
            }

            if (counter == 0)
            {
                txtRef.text = "Esperando usuario...";
            }
        }
        else // detectó toda la persona
        {
            // MANDARLO AL CENTRO
            if (!person_is_centered)
            {
                foreach (var body in data)
                {
                    if (body != null && body.IsTracked)
                    {
                        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
                        {
                            Kinect.Joint sourceJoint = body.Joints[jt];
                            Vector3 localPosition = GetVector3FromJoint(sourceJoint);
                            if (jt.ToString() == "SpineBase")
                            {
                                my_x = localPosition.x;
                                my_z = localPosition.z;
                            }
                        }
                    }
                }

                txtRef_feed.text = "";
                if (my_x >= -error_x && my_x <= error_x)//esta perfect
                {
                    txtRef.text = "Sientesé y extienda los brazos en forma de T";
                    person_is_centered = true;
                }
                else //centrar al bro
                {
                    if (my_x >= error_x)// too derecha
                    {
                        txtRef.text = "Muevete a la izquierda";
                    }
                    else
                    {
                        txtRef.text = "Muevete a la derecha";
                        Debug.Log(my_z);
                    }
                }
            }
            else
            {
                // QUE SE SIENTE Y ABRAS SUS BRAZOS, veo distancia de brazos y su Y
                if (!got_arm_lenght)//obtener el largo del brazo
                {
                    foreach (var body in data)
                    {

                        if (body != null && body.IsTracked)
                        {
                            for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
                            {
                                Kinect.Joint sourceJoint = body.Joints[jt];
                                Vector3 localPosition = GetVector3FromJoint(sourceJoint);
                                // HandLeft HandRight ShoulderRight ShoulderLeft ElbowLeft ElbowRight SpineBase
                                if (jt.ToString() == "HandLeft") 
                                {
                                    hand_l_y = localPosition.y;
                                    hand_l_x = localPosition.x;
                                }
                                if (jt.ToString() == "HandRight")
                                {
                                    hand_r_y = localPosition.y;
                                }
                                if (jt.ToString() == "ShoulderRight")
                                {
                                    shoulder_r_y = localPosition.y;
                                }
                                if (jt.ToString() == "ShoulderLeft")
                                {
                                    shoulder_l_y = localPosition.y;
                                    shoulder_l_x = localPosition.x;
                                }
                                if (jt.ToString() == "ElbowLeft")
                                {
                                    elbow_l_y = localPosition.y;
                                }
                                if (jt.ToString() == "ElbowRight")
                                {
                                    elbow_r_y = localPosition.y;
                                }
                                if (jt.ToString() == "SpineBase")
                                {
                                    final_x = localPosition.x;
                                    final_y = localPosition.y;
                                    final_z = localPosition.z;
                                }
                            }
                            ////////////////////////////////////////////////////////////////////////////////////////////////////////
                        }
                    }
                    // NO SE REVISA EL Z SOLO Y
                    // si todo tiene la misma altura (podria aumentarse ver si sale del frustrum pero es muy improbable o hasta imposible)
                    if (hand_l_y < shoulder_l_y + error_arm_y && hand_l_y > shoulder_l_y - error_arm_y) // la mano izq esta a la altura del hombro izq
                    {
                        if (elbow_l_y < shoulder_l_y + error_arm_y && elbow_l_y > shoulder_l_y - error_arm_y) // el codo izq esta a la altura del hombro izq
                        {
                            arm_lenght = Math.Abs(shoulder_l_x - hand_l_x);
                            txtRef.text = "";
                            got_arm_lenght = true;
                        }
                    }
                }
                else // TENGO TODO, PONER LA BATERIA
                {
                    if (!drums_placed)
                    {
                        //Constante del algoritmo
                        float algorithm_fixer = 10;

                        //COLOCAR LA BATERIA
                        Vector3 my_vector = new Vector3(60, 60, 60);  // SCALA NOMAS
                        Vector3 my_vector_ride = new Vector3(60, 70, 60); // SCALA PARA HACER MAS ALTO ALGUNOS
                        Vector3 algorithm_reset_position = new Vector3(0, 0, -algorithm_fixer * snare_drum_distance);//PARA MOVERLO AL ORIGGEN
                        Vector3 my_vector_high = new Vector3(-20, 0, -10); // ROTACION DE HIGH
                        Vector3 my_vector_middle = new Vector3(-20, 0, 10);// ROTACION DE MIDDLE

                        //Vector3 custom_to_user = new Vector3(final_x, -final_y - 20, final_z + arm_lenght);// ACA SE PONE LA ALTURA EN LA QUE ESTA LA PERSONA Y LA DISTANCIA DE LOS BRAZOS
                        Vector3 custom_to_user = new Vector3(final_x, -final_y - 25, -121);
                        //Vector3 custom_to_user = new Vector3(0, 0, 0);

                        Vector3 my_vector_floor = new Vector3(0, 25, 0);//ROTACION FLOOR PARA PONER EL PEDAL
                        Vector3 my_vector_snare = new Vector3(0, -30, 0);//ROTACION SNARE PARA PONER EL PEDAL

                        float middle_aux = 40;
                        float high_aux = 43;

                        float x = get_adyacent(bass_drum_distance * algorithm_fixer, bass_drum_angle);
                        GameObject bass = CreateObject(bass_drum, x, 0, bass_drum_distance * algorithm_fixer);
                        bass.transform.localScale = my_vector;
                        bass.transform.localPosition += algorithm_reset_position;
                        bass.transform.localPosition += custom_to_user;

                        x = get_adyacent(crash_distance * algorithm_fixer, crash_angle);
                        GameObject crash_obj = CreateObject(crash, x - 8, 0, crash_distance * algorithm_fixer + 17);
                        crash_obj.transform.localScale = my_vector;
                        crash_obj.transform.localPosition += algorithm_reset_position;
                        crash_obj.transform.localPosition += custom_to_user;

                        x = get_adyacent(floor_tom_distance * algorithm_fixer, floor_tom_angle);
                        GameObject floor = CreateObject(floor_tom, x, 0, floor_tom_distance * algorithm_fixer);
                        floor.transform.localScale = my_vector;
                        floor.transform.localPosition += algorithm_reset_position;
                        floor.transform.localPosition += custom_to_user;
                        floor.transform.localRotation = Quaternion.Euler(my_vector_floor);

                        x = get_adyacent(hit_hat_distance * algorithm_fixer, hit_hat_angle);
                        GameObject hit_obj = CreateObject(hit_hat, x, 0, hit_hat_distance * algorithm_fixer);
                        hit_obj.transform.localScale = my_vector;
                        hit_obj.transform.localPosition += algorithm_reset_position;
                        hit_obj.transform.localPosition += custom_to_user;

                        x = get_adyacent(high_tom_distance * algorithm_fixer, high_tom_angle);
                        GameObject high = CreateObject(high_tom, x, high_aux, high_tom_distance * algorithm_fixer);
                        high.transform.localScale = my_vector;
                        high.transform.localRotation = Quaternion.Euler(my_vector_high);
                        high.transform.localPosition += algorithm_reset_position;
                        high.transform.localPosition += custom_to_user;

                        x = get_adyacent(middle_tom_distance * algorithm_fixer, middle_tom_angle);
                        GameObject middle = CreateObject(middle_tom, x, middle_aux, middle_tom_distance * algorithm_fixer);
                        middle.transform.localScale = my_vector;
                        middle.transform.localRotation = Quaternion.Euler(my_vector_middle);
                        middle.transform.localPosition += algorithm_reset_position;
                        middle.transform.localPosition += custom_to_user;

                        x = get_adyacent(ride_distance * algorithm_fixer, ride_angle);
                        GameObject ride_obj = CreateObject(ride, x + 5, 0, ride_distance * algorithm_fixer + 8);
                        ride_obj.transform.localScale = my_vector_ride;
                        ride_obj.transform.localPosition += algorithm_reset_position;
                        ride_obj.transform.localPosition += custom_to_user;

                        x = get_adyacent(snare_drum_distance * algorithm_fixer, snare_drum_angle);
                        GameObject snare_obj = CreateObject(snare_drum, x, 0, snare_drum_distance * algorithm_fixer);
                        snare_obj.transform.localScale = my_vector;
                        snare_obj.transform.localPosition += algorithm_reset_position;
                        snare_obj.transform.localPosition += custom_to_user;
                        snare_obj.transform.localRotation = Quaternion.Euler(my_vector_snare);

                        x = get_adyacent(pedal_distance * algorithm_fixer, pedal_angle);
                        GameObject pedal_obj = CreateObject(pedal, x, 0, pedal_distance * algorithm_fixer);
                        pedal_obj.transform.localScale = my_vector;
                        pedal_obj.transform.localPosition += algorithm_reset_position;
                        pedal_obj.transform.localPosition += custom_to_user;
                        // pedal_obj.transform.localRotation = Quaternion.Euler(my_vector_snare);

                        drums_placed = true;
                    }
                    else // TODO ESTA HECHO, ACA SE TRACKEARAN COSAS EXTRA NECESARIAS
                    {
                        ///////////////////////////////
                    }
                }
                
            }

            
        }

        
    }
    
    private GameObject CreateBodyObject(ulong id)
    {
        GameObject body = new GameObject("Body:" + id);

        float scale = 0.1f;
        int scale_line = 2;
        float line_width = 0.1f;

        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            GameObject jointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            
            LineRenderer lr = jointObj.AddComponent<LineRenderer>();
            lr.SetVertexCount(scale_line);
            lr.material = BoneMaterial;
            lr.SetWidth(line_width, line_width);
            
            jointObj.transform.localScale = new Vector3(scale, scale, scale);

            jointObj.transform.position = jointObj.transform.position;
            jointObj.name = jt.ToString();
            jointObj.transform.parent = body.transform;
        }
        
        return body;
    }
    
    private void RefreshBodyObject(Kinect.Body body, GameObject bodyObject)
    {
        
        for (Kinect.JointType jt = Kinect.JointType.SpineBase; jt <= Kinect.JointType.ThumbRight; jt++)
        {
            Kinect.Joint sourceJoint = body.Joints[jt];
            Kinect.Joint? targetJoint = null;
            
            if(_BoneMap.ContainsKey(jt))
            {
                targetJoint = body.Joints[_BoneMap[jt]];
            }
            
            Transform jointObj = bodyObject.transform.FindChild(jt.ToString());
            jointObj.localPosition = GetVector3FromJoint(sourceJoint);

            LineRenderer lr = jointObj.GetComponent<LineRenderer>();
            if(targetJoint.HasValue)
            {
                lr.SetPosition(0, jointObj.localPosition);
                lr.SetPosition(1, GetVector3FromJoint(targetJoint.Value));
                lr.SetColors(GetColorForState (sourceJoint.TrackingState), GetColorForState(targetJoint.Value.TrackingState));
            }
            else
            {
                lr.enabled = false;
            }
        }

    }
    
    private static Color GetColorForState(Kinect.TrackingState state)
    {
        switch (state)
        {
        case Kinect.TrackingState.Tracked:
            return Color.green;

        case Kinect.TrackingState.Inferred:
            return Color.red;

        default:
            return Color.black;
        }
    }
    
    private static Vector3 GetVector3FromJoint(Kinect.Joint joint)
    {
        float scale = 60;
        return new Vector3(joint.Position.X * scale, joint.Position.Y * scale, -joint.Position.Z * scale);
    }
}